<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;

class SignPresenter extends BasePresenter
{
	/** @var \App\Models\Authenticator @inject */
	public $authenticator;

	protected function createComponentSignInForm()
	{
		$form = new Form();
		$form->addProtection();

		$form->addText('username','Username')
			->setAttribute('placeholder','Username');
		$form->addPassword('password','Password')
			->setAttribute('placeholder','Password');

		$form->addSubmit('ok','Login');

		$form->onSuccess[] = [$this,'signInFormSubmitted'];
		return $form;
	}

	public function signInFormSubmitted(Form $form, Array $values)
	{
		$user = $this->getUser();
		$user->setAuthenticator($this->authenticator);
		try {

			$user->login($values['username'],$values['password']);
			$user->setExpiration('14 days');
			$this->redirect('Admin:Dashboard:default');

		} catch (Nette\Security\AuthenticationException $e) {
			$form->addError($e->getMessage());
		}

	}

	public function actionOut()
	{
		$this->getUser()->logout();
		$this->redirect('Sign:in');
	}
}
