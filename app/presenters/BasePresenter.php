<?php

namespace App\Presenters;

use Nette;


abstract class BasePresenter extends Nette\Application\UI\Presenter
{

	/** @var \Nette\Mail\SmtpMailer @inject */
	public $mailer;

	public function __construct()
	{
		parent::__construct();
	}

	public function startup()
	{
		parent::startup();
		if (!$this->getUser()->isLoggedIn() && !($this->getName() == 'Sign' && $this->getAction() == 'in')) {
			$this->redirect('Sign:in');
		}
	}

	protected function beforeRender()
	{
		parent::beforeRender();
	}

	public function generateToken($length = 48)
	{
		return bin2hex(random_bytes($length/2));
	}
}
