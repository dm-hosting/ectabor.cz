<?php

namespace App\Presenters;

use Nette;

class HoubaPresenter extends Nette\Application\UI\Presenter
{

	/** @persistent */
	public $group = 'student';

	/** @var \App\Models\Houba @inject*/
	public $houba;

	/** @var Nette\Mail\SmtpMailer @inject */
	public $mailer;

	protected function createComponentHoubaRegForm()
	{
		return new \App\Components\HoubaRegForm($this->houba,$this->mailer);
	}

}
