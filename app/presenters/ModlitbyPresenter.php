<?php

namespace App\Presenters;

use Nette;

class ModlitbyPresenter extends Nette\Application\UI\Presenter
{

	public function actionDefault()
	{
		$this->template->sheetUrl = 'https://docs.google.com/spreadsheets/d/1sPRPLuFVD39xyHijatMNrh3pBbwBXFVQQLu752KJDu4/edit#gid=0';
	}

}
