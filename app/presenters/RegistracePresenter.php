<?php

namespace App\Presenters;

use Nette;

class RegistracePresenter extends Nette\Application\UI\Presenter
{

	const GROUP_TEAM = 'team';
	const GROUP_STUDENT = 'student';

	/** @persistent */
	public $group = self::GROUP_STUDENT;

	/** @var \App\Models\Registrations @inject*/
	public $registrations;

	/** @var Nette\Mail\SmtpMailer @inject */
	public $mailer;

	protected function createComponentRegForm()
	{
		return new \App\Components\RegForm($this->registrations,$this->mailer);
	}

}
