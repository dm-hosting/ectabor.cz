<?php

namespace App\Classes;

use Nextras\Forms\Rendering\Bs3FormRenderer;

class FormRenderer extends Bs3FormRenderer
{
	public function __construct()
	{
		parent::__construct();
		$this->wrappers['control']['container'] = 'div class=col-sm-10';
		$this->wrappers['label']['container'] = 'div class="col-sm-2 control-label"';
	}
}