<?php

namespace App\Components;

use App\Models\Registrations;
use Nette\Application\UI\Form;
use Nette\Mail\Message;
use Nette\Mail\SmtpException;
use Nette\Mail\SmtpMailer;
use Nette\Utils\DateTime;
use Nextras\Forms\Rendering\Bs3FormRenderer;
use \App\Presenters\RegistracePresenter;
use Tracy\Debugger;

class RegForm extends \Nette\Application\UI\Control
{

	/** @var Registrations */
	protected $registrations;

	/** @var SmtpMailer */
	protected $mailer;

	/** @persistent */
	protected $id = null;

	public function __construct(Registrations $registrations, SmtpMailer $mailer,$id = null)
	{
		parent::__construct();
		$this->registrations = $registrations;
		$this->mailer = $mailer;
		$this->setId($id);
	}

	/**
	 * @return int|null
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 * @return RegForm
	 */
	public function setId($id)
	{
		if (intval($id))
			$this->id = intval($id);

		return $this;
	}

	/**
	 * @return Form
	 */
	protected function createComponentRegForm()
	{
		if (isset($this->getPresenter()->group)) {
			/** @var string $group */
			$group = $this->getPresenter()->group;
		} else {
			$group = null;
		}

		$form = new Form();

// == OSOBNÍ ÚDAJE

		$form->addGroup('Osobní údaje');

		$form->addText('name','Jméno')
			->addRule(Form::FILLED,'Vyplň jméno');
		$form->addText('surname','Příjmení')
			->addRule(Form::FILLED,'Vyplň příjmení');
		$form->addDatePicker('birth_date','Datum narození')
			->addRule(Form::FILLED,'Vyplň datum narození');
		$form->addRadioList('sex','Pohlaví',['male'=>'Muž','female'=>'Žena'])
			->addRule(Form::FILLED,'Vyber pohlaví');
		$form->addText('address','Adresa')
			->setOption("description", \Nette\Utils\Html::el("span class=help-block")->setHtml('Ulice + č.p.'))
			->addRule(Form::FILLED,'Vyplň adresu');
		$form->addText('city','Město')
			->addRule(Form::FILLED,'Vyplň město');
		$form->addInteger('zip','PSČ')
			->addRule(Form::FILLED,'Vyplň PSČ')
			->addRule(Form::PATTERN,'PSČ musí být pětimístné číslo','\d{5}');
		/*$form->addInteger('personal_id','Číslo občanského průkazu')
			->setOption("description", \Nette\Utils\Html::el("span class=help-block")->setHtml('Pokud ještě nemáš občanku, žádné si nevymýšlej a zadej jen několik nul'))
			->addRule(Form::FILLED,'Vyplň číslo OP');*/
		$form->addTextArea('health_issues','Zdravotní stav')
			->setOption("description", \Nette\Utils\Html::el("span class=help-block")->setHtml('např.: alergie, astma, epilepsie, zvláštní strava ...'))
			->setAttribute('rows',4);


// == KONTAKT

		$form->addGroup('Kontakt');
		$form->addEmail('email','Email')
			->setOption("description", \Nette\Utils\Html::el("span class=help-block")->setHtml('Na tento email ti po přihlášení budou chodit všechny informace o kempu'))
			->addRule(Form::FILLED,'Vyplň email')
			->addRule(Form::EMAIL,'Email je ve špatném formátu');
		$form->addText('phone','Telefon')
			->setDefaultValue('+420')
			->addRule(Form::FILLED,'Vyplň telefon')
			->addRule(Form::PATTERN,'Telefon vyplň bez mezer v mezinárodním formátu, např.: +420123456789','\+420\d{9}');
		$form->addText('parent','Jméno zákonného zástupce')
			->setRequired(false)
			->setOption("description", \Nette\Utils\Html::el("span class=help-block")->setHtml('Pokud jsi nezletilý, vyplň koho máme kontaktovat v případě úrazu'));
		$form->addText('parent_phone','Telefon zákonného zástupce')
			->setRequired(false)
			->setDefaultValue('+420')
			->addConditionOn($form['parent'],Form::FILLED)
			->addRule(Form::PATTERN,'Telefon vyplň bez mezer v mezinárodním formátu, např.: +420123456789','\+420\d{9}');


// == NA KEMPU

		$form->addGroup('Na kempu');
		$form->addText('room_with','Rád/a bych byl/a na pokoji s:')
			->setOption("description", \Nette\Utils\Html::el("span class=help-block")->setHtml('Pokud bys byl/a rád/a s někým ubytovaný/á na pokoji, tak nám to sem můžeš napsat a my se to pokusíme zařídit'));
		$form->addCheckboxList('transportation','Mám zájem o dopravu',['to'=>'Na kemp','from'=>'Z kempu'])
			->setOption("description", \Nette\Utils\Html::el("span class=help-block")->setHtml('Z Tábora na místo campu pojede autobus, takže se nemusíš starat o to, jak se dostaneš na camp a zpět'));

		$form->addRadioList('english','Moje úroveň angličtiny je',[1=>'Mohl/a bych překládat',2=>'Domluvím se',3=>'Trochu anglicky umím',4=>'Vůbec anglicky neumím'])
			->addRule(Form::FILLED,'Vyber tvojí úroveň angličtiny');

		// trička
		if ($group == RegistracePresenter::GROUP_TEAM || $this->id) {
			$form->addRadioList('shirt_sex','Tričko',['female'=>'Dámské','male'=>'Pánské'])
				->setOption("description", \Nette\Utils\Html::el("span class=help-block")
					->setHtml('Tričko je letos garantované pouze pro tebe, jako člena organizačního týmu EC a budeš ho mít zdarma.'));
				//->addRule(Form::FILLED,'Vyber typ trička');
			$form->addRadioList('shirt_size','Tričko',['S'=>'S','M'=>'M','L'=>'L','XL'=>'XL','XXL'=>'XXL']);
				//->addRule(Form::FILLED,'Vyber velikost trička');
		}
		//$form->addCheckbox('shirt_reservation','Chci zarezervovat tričko (+150 Kč)')
		//	->setOption("description", \Nette\Utils\Html::el("span class=help-block")->setHtml('Tričko bude možné koupit na kempu i bez rezervace, ale když si ho zarezervuješ, bude na tebe čekat připravené u registrace'));




		$form->addRadioList('workshop','Jako dopolední workshop chci',['airsoft'=>'Airsoft','english'=>'Angličtinu','drama'=>'Herectví'])
			->setOption("description", \Nette\Utils\Html::el("span class=help-block")->setHtml('Englishcamp je MULTI Camp - můžeš si vybrat, jestli se chceš dopoledne učit angličtinu, hrát airsoft nebo se učit, jak hrát scénky'))
			->addRule(Form::FILLED,'Vyber si workshop')
			->addCondition($form::EQUAL,'airsoft')
			->toggle('form-airsoft');


// == AIRSOFT

		$form->addGroup('Airsoft')
			->setOption("container", \Nette\Utils\Html::el("fieldset")->id('form-airsoft'));
		$form->addRadioList('airsoft_equipment','Vybavení na airsoft',['borrow'=>'Chci si ho půjčit (+500 Kč)','have'=>'Mám svoje'])
			->setOption("description", \Nette\Utils\Html::el("span class=help-block")->setHtml('Pokud máš zájem, můžeme ti na campu půjčit celou výbavu, takže si budeš moct zahrát, aniž by ses o cokoli staral'))
			->addConditionOn($form['workshop'],Form::EQUAL,'airsoft')
			->addRule(Form::FILLED,'Vyber zda máš vybavení nebo ho chceš půjčit od nás');
		$form->addCheckboxList('airsoft_game','Typ hry',['bitka'=>'Bitka (ať kuličky lítaji)','ctf'=>'Capture the flag','military'=>'Military airsoft (scénáře)','celokempova-mise'=>'Celokempová mise (Tajná mise pro každého na celý kemp)'])
			->setOption("description", \Nette\Utils\Html::el("span class=help-block")->setHtml('Pokud preferuješ nějaký styl hry airsoftu, vyplň nám ho prosím, pomůže nám to při přípravě programu'));

// == POZNÁMKA
		$form->addGroup('Poznámka');
		$form->addTextArea('note','')
			->setOption("description", \Nette\Utils\Html::el("span class=help-block")->setHtml('Prostor pro cokoliv, co bys chtěl/a sdělit organizátorům.'))
			->setAttribute('rows',4);

		if (!$this->id) {
// == ZPRACOVÁNÍ OS. ÚDAJŮ
			$form->addGroup('Zpracování osobních údajů');
			$form->addCheckbox('szou','Souhlasím se zpracováním osobních údajů')
				->setOption("description", \Nette\Utils\Html::el("p class=help-block")->setHtml('Prohlašuji, že jestliže mi je méně než 16 let, tak jsem požádal svého zákonného zástupce (rodiče) o souhlas se zpracováním mých osobních údajů.<br><a data-toggle="modal" data-target="#szouModal">Více o zpracování osobních údajů</a>'))
				->addRule(Form::FILLED,'Musíš souhlasit se zpracováním osobních údajů');

			$form->addCheckbox('szzs','Výslovně souhlasím se zpracováním údajů o zdravotním stavu')
				->addRule(Form::FILLED,'Musíš souhlasit se zpracováním údajů o zdravotním stavu');

		} else {
			$form->addGroup('Skupina a cena');
			$form->addSelect('group','Skupina',['student'=>'Student','team'=>'Tým'])
				->setPrompt('-- Vyber --')
				->addRule(Form::FILLED,'Vyber skupinu');
			$form->addText('price','Cena')
				->addRule(Form::FILLED,'Vyplň cenu')
				->addRule(Form::INTEGER,'Cena musí být celé číslo')
				->addRule(Form::MAX_LENGTH,'MAximální počet číslic v ceně je %d',8);
			$form->addGroup('Interní poznámka');
			$form->addTextArea('internal_note','')
				->setOption("description", \Nette\Utils\Html::el("span class=help-block")->setHtml('Viditelná pouze pro lidi s přístupem do této administrace'))
				->setAttribute('rows',10);
			$form->addSelect('payment_method','Metoda platby',['transaction'=>'Převod na účet','cash'=>'Hotovost','fksp'=>'FKSP']);
		}

		$form->addSubmit('send','Odeslat');

		if ($this->id) {
			$form->addSubmit('cancel','Zrušit')
				->setValidationScope(false);
			$form->setAction('?id='.$this->id);
		}

		$form->setRenderer(new Bs3FormRenderer());
		$form->setDefaults($this->getDefaults());

		$form->onSuccess[] = [$this,'RegFormSuccess'];

		return $form;
	}

	public function RegFormSuccess(Form $form, $values)
	{

		if ($form['send']->isSubmittedBy()) {

			//doprava + AS
			$values->transportation = implode(',',$values->transportation);
			$values->airsoft_game = implode(',',$values->airsoft_game);

			if (!$this->id) {
				//skupina
				$values->group = $this->getGroup();

				//cena
				$values->price = 3650;

				if (date('Ymd') >= 20190601)
					$values->price = $values->price + 200;
				/*if ($values->shirt_reservation && $values->group!='team')
					$values->price += 150;*/
				if ($values->airsoft_equipment == 'borrow')
					$values->price += 500;

				$values->created = new DateTime();
			}

			\Tracy\Debugger::log($values);

			if ($this->id) {

				$values->update = new DateTime();
				if ($this->registrations->update($this->id,$values)) {
					$this->getPresenter()->flashMessage('Registrace byla upravena','success');
				} else {
					$this->getPresenter()->flashMessage('Nepovedlo se uložit úpravy v registraci','danger');
				}

				$this->getPresenter()->redirect('Dashboard:default');

			} else {

				if ($registration = $this->registrations->insert($values)) {
					$mail = new Message();
					$mail->setFrom('david@ectabor.cz')
						->addTo($values->email)
						->addReplyTo('eliskacmel@gmail.com')
						->setSubject('Registrace na English Camp')
						->setHtmlBody('
					<p>Ahoj,<br>
					děkujeme za tvoji registraci na English Camp. <strong>Aby byla tvoje registrace platná, je potřeba kemp zaplatit do 14-ti dnů od přihlášení.</strong> Tady jsou údaje potřebné k provedení platby.</p>
					<p>
						<strong>Číslo účtu: </strong>1409419012/3030<br>
						<strong>Variabliní symbol: </strong>'.str_replace('+420','',$values->phone).'<br>
						<strong>Poznámka pro příjemce: </strong>'.$values->name.' '.$values->surname.'<br>
						<strong>Částka: </strong>'.$values->price.' Kč
					</p>
					<p>Platbu je potřeba uskutečnit nejpozději do 30.6.'.date('Y').', ale kdybychom vyčerpali kapacitu kempu, tak platí, že kdo má dříve zaplaceno, jede.<br>
					Jakmile tvoji platbu obdržíme, ozveme se ti s dalšími informacemi.</p>
					<p>Pokud by ses po zaplacení rozhodl odhlásit, peníze ti vrátíme, ale bude z nich odečten storno poplatek. Ten do 30.6.'.date('Y').' činí 50% procent ze zaplacené částky a od 1.7.'.date('Y').' 100% zaplacené částky.</p>
					<p>Pokud bys měl/a jakékoliv dotazy ohledně platby nebo registrace, odpověz na tento mail, nebo napiš Elišce Čmelíkové na <a href="mailto:eliskacmel@gmail.com">eliskacmel@gmail.com</a></p>
					<p>S pozdravem,<br>
					David Macek</p> 
				');

					try {
						$this->mailer->send($mail);
						$this->registrations->update($registration->id,['reg_mail_sent'=>new DateTime(),'status'=>'reg_mail_sent']);
						$this->getPresenter()->redirect('Registrace:success');
					} catch (SmtpException $e) {
						\Tracy\Debugger::log($e);
						$this->getPresenter()->redirect('Registrace:errorMail');
					}

				} else {
					$this->getPresenter()->redirect('Registrace:errorSave');
				}

			}

		} else {
			$this->getPresenter()->redirect('Dashboard:default');
		}

	}

	/**
	 * @return mixed|string
	 */
	protected function getGroup()
	{
		if (isset($this->getPresenter()->group))
			return $this->getPresenter()->group;
		else
			return 'student';
	}

	/**
	 * @return array
	 */
	protected function getDefaults()
	{
		if ($this->id && $registration = $this->registrations->findBy(['id'=>$this->id])->fetch()) {
			$result = $registration->toArray();
			if (strlen($result['transportation'])>0)
				$result['transportation'] = explode(',',$result['transportation']);
			else
				$result['transportation'] = [];
			if (strlen($result['airsoft_game'])>0)
				$result['airsoft_game'] = explode(',',$result['airsoft_game']);
			else
				$result['airsoft_game'] = [];

			return $result;
		} else {
			return [];
		}
	}

	public function render()
	{
		$this->template->setFile(__DIR__.'/templates/RegForm.latte');
		$this->template->render();
	}
}