<?php

namespace App\Components;

use App\Models\Houba;
use Nette\Application\UI\Form;
use Nette\Mail\Message;
use Nette\Mail\SmtpException;
use Nette\Mail\SmtpMailer;
use Nette\Utils\DateTime;
use Nextras\Forms\Rendering\Bs3FormRenderer;
use Tracy\Debugger;

class HoubaRegForm extends \Nette\Application\UI\Control
{

	/** @var Houba */
	protected $houba;

	/** @var SmtpMailer */
	protected $mailer;

	/** @persistent */
	protected $id = null;

	public function __construct(Houba $houba, SmtpMailer $mailer,$id = null)
	{
		parent::__construct();
		$this->houba = $houba;
		$this->mailer = $mailer;
		$this->setId($id);
	}

	/**
	 * @return int|null
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 * @return RegForm
	 */
	public function setId($id)
	{
		if (intval($id))
			$this->id = intval($id);

		return $this;
	}

	/**
	 * @return Form
	 */
	protected function createComponentRegForm()
	{
		$form = new Form();

		$form->addGroup('Osobní údaje');

		$form->addText('name','Jméno a příjmení')
			->addRule(Form::FILLED,'Vyplň jméno a příjmení');

		$form->addRadioList('sex','Pohlaví',$this->houba->getSex())
			->addRule(Form::FILLED,'Vyber pohlaví');

		$form->addEmail('email','Email')
			->setOption("description", \Nette\Utils\Html::el("span class=help-block")->setHtml('Na tento email ti po přihlášení budou chodit všechny informace o Houbě 2019'))
			->addRule(Form::FILLED,'Vyplň email')
			->addRule(Form::EMAIL,'Email je ve špatném formátu');

		$form->addSelect('youthgroup','Mládež',$this->houba->getYouthGroups())
			->addRule(Form::FILLED,'Vyber z jaké jsi mládeže')
			->setPrompt('--- Vyber ---');

		$form->addGroup('Jídlo');

		$form->addRadioList('days','Na jak dlouho přijedeš?',$this->houba->getDays())
			->addRule(Form::FILLED,'Vyber, na jak dlouho přijedeš');

		$form->addGroup('Ubytování');

		$form->addRadioList('sleep','Máme ti zajistit ubytování?',[1=>'Ano',0=>'Ne'])
			->setOption("description", \Nette\Utils\Html::el("span class=help-block")->setHtml('Ubytování budeme zajišťovat v naší sborové budově - nezapomeň s sebou spacák a karimatku ;)'))
			->addRule(Form::FILLED,'Vyber, zda chceš zajistit přespání');


// == POZNÁMKA
		$form->addGroup('Poznámka');
		$form->addTextArea('note','')
			->setOption("description", \Nette\Utils\Html::el("span class=help-block")->setHtml('Prostor pro cokoliv, co bys chtěl/a sdělit organizátorům.'))
			->setAttribute('rows',4);


// == ZPRACOVÁNÍ OS. ÚDAJŮ
			$form->addGroup('Zpracování osobních údajů');
			$form->addCheckbox('szou','Souhlasím se zpracováním osobních údajů')
				->setOption("description", \Nette\Utils\Html::el("p class=help-block")->setHtml('Prohlašuji, že jestliže mi je méně než 16 let, tak jsem požádal svého zákonného zástupce (rodiče) o souhlas se zpracováním mých osobních údajů.<br><a data-toggle="modal" data-target="#szouModal">Více o zpracování osobních údajů</a>'))
				->addRule(Form::FILLED,'Musíš souhlasit se zpracováním osobních údajů');

		$form->addSubmit('send','Odeslat');

		$form->setRenderer(new Bs3FormRenderer());

		$form->onSuccess[] = [$this,'RegFormSuccess'];

		return $form;
	}

	public function RegFormSuccess(Form $form, $values)
	{

		if ($form['send']->isSubmittedBy()) {

			//price
			$prices = [
				'all' => 180,
				'pa' => 50,
				'so' => 130
			];

			$values->price = $prices[$values['days']];

			//szou
			$values->szou = new DateTime();

			$values->created = new DateTime();

			\Tracy\Debugger::log($values);


			if ($registration = $this->houba->insert($values)) {
				$mail = new Message();
				$mail->setFrom('david@ectabor.cz')
					->addTo($values->email)
					//->addReplyTo('eliskacmel@gmail.com')
					->setSubject('Registrace na Houbu 2019')
					->setHtmlBody('
					<p>Ahoj,<br>
					děkujeme za tvoji registraci na Houbu 2019. <strong>Zaplatit můžeš buď předem nebo na místě.</strong> Tady jsou údaje potřebné k provedení platby předem.</p>
					<p>
						<strong>Číslo účtu: </strong>1409419012/3030<br>
						<strong>Variabliní symbol: </strong>' . sprintf('2019%03d',$registration->id) . '<br>
						<strong>Poznámka pro příjemce: </strong>' . $values->name . '<br>
						<strong>Částka: </strong>' . $values->price . ' Kč
					</p>
					<p>Platbu je potřeba uskutečnit nejpozději do 2.4.' . date('Y') . '<br>
					Jakmile tvoji platbu obdržíme, dáme ti vědět.</p>
					<p>Pokud bys měl/a jakékoliv dotazy ohledně platby nebo registrace, odpověz na tento mail.</p>
					<p>Za Táborskou mládež zdraví,<br>
					David Macek</p> 
				');

				try {
					$this->mailer->send($mail);
					$this->houba->update($registration->id, ['reg_mail_sent' => new DateTime()]);
					$this->getPresenter()->redirect('Houba:success');
				} catch (SmtpException $e) {
					\Tracy\Debugger::log($e);
					$this->getPresenter()->redirect('Houba:errorMail');
				}

			} else {
				$this->getPresenter()->redirect('Houba:errorSave');
			}

		}

	}

	public function render()
	{
		$this->template->setFile(__DIR__.'/templates/HoubaRegForm.latte');
		$this->template->render();
	}
}