<?php
namespace App\Models;

use Nette;

class Registrations extends Base
{
	/**
	 * @return Nette\Database\Table\Selection
	 */
	public function findValid()
	{
		return $this->findBy(['invalidated IS NULL']);
	}

	public function getColumns()
	{
		return [
			'id' => 'ID',
			'name' => 'Jméno',
			'surname' => 'Příjmení',
			'birth_date' => 'Datum narození',
			'sex' => 'Pohlaví',
			'address' => 'Adresa',
			'city' => 'Město',
			'zip' => 'PSČ',
			'health_issues' => 'Zdravotní problémy',
			'email' => 'Email',
			'phone' => 'Telefon',
			'parent' => 'Jméno rodiče',
			'parent_phone' => 'Telefon an rodiče',
			'room_with' => 'Chce být na pokoji s',
			'transportation' => 'Doprava',
			'english' => 'Angličtina',
			'shirt_sex' => 'Tričko',
			'shirt_size' => 'Velikost trička',
			'shirt_reservation' => 'Rezervace trička',
			'workshop' => 'Workshop',
			'airsoft_equipment' => 'Vybavení na airsoft',
			'airsoft_game' => 'Airsoft - typ hry',
			'note' => 'Poznámka',
			'internal_note' => 'Interní poznámka',
			'price' => 'Cena',
			'group' => 'Skupina',
			'status' => 'Status',
			'created' => 'Datum přihlášení'
		];
	}
}