<?php
namespace App\Models;

use Nette\Security;

class Authenticator implements Security\IAuthenticator
{
	/**
	 * @var \App\Models\Users
	 */
	protected $users;

	function __construct(Users $users)
	{
		$this->users = $users;
	}

	/**
	 * @param array $credentials
	 * @return Security\Identity
	 * @throws Security\AuthenticationException
	 */
	function authenticate(array $credentials)
	{
		list($username, $password) = $credentials;
		$row = $this->users->findBy(['username'=>$username])->fetch();

		if (!$row) {
			throw new Security\AuthenticationException('User not found.');
		}

		if (!Security\Passwords::verify($password, $row->password)) {
			throw new Security\AuthenticationException('Invalid password.');
		}

		return new Security\Identity($row->id, $row->role, $row);
	}
}