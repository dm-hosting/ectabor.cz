<?php
namespace App\Models;

use Nette;

class Houba extends Base
{
	/**
	 * @return Nette\Database\Table\Selection
	 */
	public function findValid()
	{
		return $this->findBy(['invalidated IS NULL']);
	}

	public function getColumns()
	{
		return [
			'id' => 'ID',
			'name' => 'Jméno a příjmení',
			'email' => 'Email',
			'youthgroup' => 'Mládež',
			'price' => 'Cena',
			'sleep' => 'Přespání',
			'paied' => 'Zaplaceno',
			'created' => 'Datum přihlášení'
		];
	}

	public function getYouthGroups()
	{
		return [
			'CB České Budějovice' => 'CB České Budějovice',
			'CB Český Krumlov' => 'CB Český Krumlov',
			'CB Husinec' => 'CB Husinec',
			'CB Písek' => 'CB Písek',
			'ELIM Písek' => 'ELIM Písek',
			'CB Podblanicko' => 'CB Podblanicko',
			'ELIM Strakonice' => 'ELIM Strakonice',
			'CB Tábor' => 'CB Tábor',
			'jiné' => 'jiné'
		];
	}

	public function getSex()
	{
		return [
			'male'=>'Muž',
			'female'=>'Žena',
			'other'=>'Jiné'
		];
	}

	public function getDays()
	{
		return [
			'all' => 'Na celou Houbu (180Kč)',
			'pa' => 'Pouze na pátek (50Kč)',
			'so' => 'Pouze na sobotu (130Kč)'
		];
	}
}