<?php

namespace App\AdminModule\Presenters;

use Nette;
use Nette\Application\UI\Form;
use Ublaboo\DataGrid\DataGrid;

class DashboardPresenter extends \App\Presenters\BasePresenter
{

	/** @var \App\Models\Registrations @inject*/
	public $registrations;

	/**
	 * @return DataGrid
	 * @throws \Ublaboo\DataGrid\Exception\DataGridException
	 */
	protected function createComponentDatagridOverview()
	{
		$grid = new DataGrid();
		$grid->setDataSource($this->registrations->findValid());
		$grid->setColumnsHideable();
		$grid->setDefaultPerPage(100);
		$grid->setDefaultSort(['created'=>'DESC']);


		$grid->addColumnNumber('id','ID')
			->setSortable()
			->setDefaultHide();
		$grid->addColumnText('name','Jméno')
			->setSortable()
			->setRenderer(function ($item) {
				return $item->name.' '.$item->surname;
			})
			->setFilterText(['name','surname']);
		$grid->addColumnDateTime('birthDate','Datum narození','birth_date')
			->setSortable()
			->setDefaultHide()
			->setFilterDateRange();

		$grid->addColumnNumber('age','Věk','birth_date')
			->setSortable()
			->setRenderer(function ($item) {
				$dob = $item->birth_date;
				$now = new \DateTime();
				$difference = $now->diff($dob);
				$age = $difference->y;
				return $age;
			});

		$grid->addColumnText('sex','Pohlaví')
			->setSortable()
			->setDefaultHide()
			->setRenderer(function ($item) {
				if ($item->sex == 'male')
					return 'Muž';
				else
					return 'Žena';
			})
			->setFilterSelect([''=>'Vše','male'=>'Muž','female'=>'Žena']);

		$grid->addColumnText('address','Adresa')
			->setSortable()
			->setDefaultHide()
			->setRenderer(function ($item) {
				return $item->address.', '.$item->zip.' '.$item->city;
			})
			->setFilterText(['address','city','zip']);

		$grid->addColumnNumber('personalId','Číslo OP','personal_id')
			->setDefaultHide()
			->setFilterText();

		$grid->addColumnText('healthIssues','Zdravotní problémy','health_issues')
			->setDefaultHide();

		$grid->addColumnText('email','Email')
			->setSortable()
			->setFilterText();

		$grid->addColumnText('phone','Telefon')
			->setSortable()
			->setFilterText();

		$grid->addColumnText('parent','Jméno rodiče')
			->setSortable()
			->setDefaultHide()
			->setFilterText();

		$grid->addColumnText('parent_phone','Telefon na rodiče')
			->setSortable()
			->setDefaultHide()
			->setFilterText();

		$grid->addColumnText('room_with','Na pokoji s')
			->setDefaultHide()
			->setFilterText();

		$grid->addColumnText('transportation','Doprava')
			->setSortable()
			->setDefaultHide()
			->setRenderer(function ($item) {
				if ($item->transportation == 'to')
					return 'Na kemp';
				elseif ($item->transportation == 'from')
					return 'Z kempu';
				elseif ($item->transportation == 'to,from')
					return 'Obě cesty';
				else
					return 'Žádnou';
			})
			->setFilterSelect([''=>'Vše','to'=>'Na kemp','from'=>'Z kempu','to,from'=>'Obě cesty']);

		$grid->addColumnNumber('english','Úroveň angličtiny')
			->setSortable()
			->setDefaultHide()
			->setFilterText();

		$grid->addColumnText('shirtSex','Tričko','shirt_sex')
			->setSortable()
			->setDefaultHide()
			->setRenderer(function ($item) {
				if ($item->shirt_sex == 'male')
					return 'Pánské';
				else
					return 'Dámské';
			})
			->setFilterSelect([''=>'Vše','male'=>'Pánské','female'=>'Dámské'],'shirt_sex');

		$grid->addColumnText('shirtSize','Velikost trička','shirt_size')
			->setSortable()
			->setDefaultHide()
			->setFilterSelect([''=>'Vše','S'=>'S','M'=>'M','L'=>'L','XL'=>'XL','XXL'=>'XXL'],'shirt_size');

		$grid->addColumnText('shirtReservation','Rezervace trička','shirt_reservation')
			->setSortable()
			->setDefaultHide()
			->setRenderer(function ($item) {
				if ($item->shirt_reservation == 1)
					return 'Ano';
				else
					return 'Ne';
			})
			->setFilterSelect([''=>'Vše',0=>'Ne',1=>'Ano'],'shirt_reservation');

		$grid->addColumnText('workshop','Workshop')
			->setSortable()
			->setRenderer(function ($item) {
				if ($item->workshop == 'airsoft')
					return 'Airsoft';
				elseif ($item->workshop == 'drama')
					return 'Herectví';
				else
					return 'Angličtina';
			})
			->setFilterSelect([''=>'Vše','airsoft'=>'Airsoft','english'=>'Angličtina','drama'=>'Herectví']);

		$grid->addColumnText('airsoftEquipment','Vybavení na AS','airsoft_equipment')
			->setSortable()
			->setDefaultHide()
			->setRenderer(function ($item) {
				if ($item->airsoft_equipment == 'borrow')
					return 'Půjčit';
				elseif ($item->airsoft_equipment == 'have')
					return 'Vlastní';
				else
					return '';
			})
			->setFilterSelect([''=>'Vše','borrow'=>'Půjčit','have'=>'Vlastní']);

		$grid->addColumnText('airsoftGame','Typ hry AS','airsoft_game')
			->setSortable()
			->setDefaultHide()
			->setFilterText();

		$grid->addColumnNumber('price','Cena')
			->setSortable()
			->setFilterText();

		$grid->addColumnText('group','Skupina')
			->setSortable()
			->setDefaultHide()
			->setFilterSelect([''=>'Vše','student'=>'Studenti','team'=>'Tým']);

		$grid->addColumnText('note','Poznámka');

		$grid->addColumnText('internalNote','Interní poznámka','internal_note');

		$grid->addColumnText('paymentMethod','Metoda platby','payment_method')
			->setSortable()
			->setReplacement(['transaction'=>'Převod na účet','cash'=>'Hotovost','fksp'=>'FKSP']);

		$grid->addFilterSelect('paymentMethod','Metoda platby',[''=>'Vše','transaction'=>'Převod na účet','cash'=>'Hotovost','fksp'=>'FKSP'],'payment_method');

		$grid->addColumnDateTime('created','Datum přihlášení')
			->setSortable()
			->setDefaultHide()
			->setFilterDateRange();

		$grid->addColumnStatus('status', 'Status')
			->setSortable()
			->addOption('no_action', 'Chyba registrace')
				->setClass('btn-danger')
				->endOption()
			->addOption('reg_mail_sent', 'Odeslán reg. mail')
				->setClass('btn-default')
				->endOption()
			->addOption('paied', 'Zaplaceno')
			    ->setClass('btn-warning')
				->endOption()
			->addOption('pay_mail_sent', 'Odeslán info mail')
				->setClass('btn-success')
				->endOption()
			->onChange[] = [$this, 'statusChange'];

		$grid->addAction('Registration:edit', '')
			->setIcon('pencil')
			->setTitle('Upravit')
			->setClass('btn btn-xs btn-primary');

		$grid->addAction('sendPaymentMail', '', 'sendPaymentMail!')
			->setIcon('envelope')
			->setTitle('Znovu odeslat mail s informacemi o platbě')
			->setClass('btn btn-xs btn-default')
			->setConfirm('Opravdu chceš znovu odeslat mail s informacemi o platbě pro %s?', 'name');

		$grid->addAction('delete', '', 'delete!')
			->setIcon('trash')
			->setTitle('Smazat')
			->setClass('btn btn-xs btn-danger ajax')
			->setConfirm('Oprvadu chceš odstranit registraci účastníka %s?', 'name');

		$grid->addExportCsvFiltered('Export vyfiltrovaných', 'camp_registrace.csv','windows-1250');
		$grid->addExportCsv( 'Export všeho', 'camp_registrace_all.csv','windows-1250');

		return $grid;
	}

	public function statusChange($id, $newStatus)
	{
		if ($newStatus != 'no_action')
			$this->registrations->update($id,[$newStatus=>new Nette\Utils\DateTime(),'status'=>$newStatus]);

		if ($this->isAjax()) {
			$this['datagridOverview']->redrawItem($id);
		}
	}

	public function handleDelete($id)
	{
		$this->registrations->update($id,['invalidated'=>new Nette\Utils\DateTime()]);

		$this['datagridOverview']->reload();
	}

	public function handleSendPaymentMail($id)
	{
		if ($registration = $this->registrations->findBy(['id'=>$id])->fetch()) {

			$mail = new Nette\Mail\Message();
			$mail->setFrom('david@ectabor.cz')
				->addTo($registration->email)
				->addReplyTo('eliskacmel@gmail.com')
				->setSubject('Registrace na English Camp')
				->setHtmlBody('
					<p>Ahoj,<br>
					děkujeme za tvoji registraci na English Camp. <strong>Aby byla tvoje registrace platná, je potřeba kemp zaplatit.</strong> Tady jsou údaje potřebné k provedení platby.</p>
					<p>
						<strong>Číslo účtu: </strong>1409419012/3030<br>
						<strong>Variabliní symbol: </strong>'.str_replace('+420','',$registration->phone).'<br>
						<strong>Poznámka pro příjemce: </strong>'.$registration->name.' '.$registration->surname.'<br>
						<strong>Částka: </strong>'.$registration->price.' Kč
					</p>
					<p>Platbu je potřeba uskutečnit nejpozději do 30.6.'.date('Y').', ale kdybychom vyčerpali kapacitu kempu, tak platí, že kdo má dříve zaplaceno, jede.<br>
					Jakmile tvoji platbu obdržíme, ozveme se ti s dalšími informacemi.</p>
					<p>Pokud bys měl/a jakékoliv dotazy ohledně platby nebo registrace, odpověz na tento mail, nebo napiš Elišce Čmelíkové na <a href="mailto:eliskacmel@gmail.com">eliskacmel@gmail.com</a></p>
					<p>S pozdravem,<br>
					David Macek</p> 
				');

			try {
				$this->mailer->send($mail);
				$this->getPresenter()->redirect('resendMailSuccess');
			} catch (Nette\Mail\SmtpException $e) {
				\Tracy\Debugger::log($e);
				$this->getPresenter()->redirect('resendMailError');
			}

		} else {
			throw new Nette\InvalidArgumentException('Wrong registration ID was passed');
		}
	}

}
