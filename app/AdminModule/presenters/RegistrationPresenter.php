<?php

namespace App\AdminModule\Presenters;

use App\Components\RegForm;
use Nette\Application\BadRequestException;

class RegistrationPresenter extends \App\Presenters\BasePresenter
{

	/** @var \App\Models\Registrations @inject*/
	public $registrations;

	/**
	 * @return RegForm
	 * @throws BadRequestException
	 */
	protected function createComponentRegForm()
	{
		if ($this->getParameter('id'))
			return new RegForm($this->registrations,$this->mailer,$this->getParameter('id'));
		else
			throw new BadRequestException('Není zadané ID registrace');
	}

}
