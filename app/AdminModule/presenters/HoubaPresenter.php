<?php

namespace App\AdminModule\Presenters;

use Nette;
use Nette\Application\UI\Form;
use Ublaboo\DataGrid\DataGrid;

class HoubaPresenter extends \App\Presenters\BasePresenter
{

	/** @var \App\Models\Houba @inject*/
	public $houba;

	/**
	 * @return DataGrid
	 * @throws \Ublaboo\DataGrid\Exception\DataGridException
	 */
	protected function createComponentDatagridOverview()
	{
		$grid = new DataGrid();
		$grid->setDataSource($this->houba->findValid());
		$grid->setColumnsHideable();
		$grid->setDefaultPerPage(100);
		$grid->setDefaultSort(['created'=>'DESC']);


		$grid->addColumnNumber('id','ID')
			->setSortable()
			->setDefaultHide();
		$grid->addColumnText('name','Jméno a příjmení')
			->setSortable()
			->setFilterText();

		$grid->addColumnText('email','Email')
			->setSortable()
			->setFilterText();

		$grid->addColumnText('sex','Pohlaví')
			->setSortable()
			->setRenderer(function ($item) {
				$model = $this->houba->getSex();
				return $model[$item->sex];
			})
			->setFilterSelect(array_merge([''=>'Vše'],$this->houba->getSex()));

		$grid->addColumnText('youthgroup','Mládež')
			->setSortable()
			->setFilterSelect(array_merge([''=>'Vše'],$this->houba->getYouthGroups()));

		$grid->addColumnText('days','Dny')
			->setSortable()
			->setRenderer(function ($item) {
				$model = $this->houba->getDays();
				return $model[$item->days];
			})
			->setFilterSelect(array_merge([''=>'Vše'],$this->houba->getDays()));

		$grid->addColumnNumber('price','Cena')
			->setSortable();

		$grid->addColumnText('sleep','Přespání')
			->setSortable()
			->setRenderer(function ($item) {
				if ($item->sleep == 1)
					return 'Ano';
				else
					return 'Ne';
			})
			->setFilterSelect([''=>'Vše',1=>'Ano',0=>'ne']);

		$grid->addColumnText('note','Poznámka')
			->setSortable();

		$grid->addColumnDateTime('created','Datum přihlášení')
			->setSortable()
			->setDefaultHide()
			->setFilterDateRange();

		$grid->addColumnStatus('paied', 'Zaplaceno')
			->setSortable()
			->addOption(0, 'Nezaplaceno')
				->setClass('btn-warning')
				->endOption()
			->addOption(1, 'Zaplaceno')
				->setClass('btn-success')
				->endOption()
			->onChange[] = [$this, 'paiedChange'];

		$grid->addColumnStatus('arrived', 'Zaregistrován')
			->setSortable()
			->addOption(0, 'Ne')
			->setClass('btn-default')
			->endOption()
			->addOption(1, 'Ano')
			->setClass('btn-success')
			->endOption()
			->onChange[] = [$this, 'arrivedChange'];

		$grid->addAction('delete', '', 'delete!')
			->setIcon('trash')
			->setTitle('Smazat')
			->setClass('btn btn-xs btn-danger ajax')
			->setConfirm('Oprvadu chceš odstranit registraci účastníka %s?', 'name');

		$grid->addExportCsvFiltered('Export vyfiltrovaných', 'houba_registrace.csv','windows-1250');
		$grid->addExportCsv( 'Export všeho', 'houba_registrace_all.csv','windows-1250');

		return $grid;
	}

	public function paiedChange($id, $newStatus)
	{
		$this->houba->update($id,['paied'=>$newStatus]);
		/*if ($newStatus==1)
			$this->handleSendPaymentMail($id);*/

		if ($this->isAjax()) {
			$this['datagridOverview']->redrawItem($id);
		}
	}

	public function arrivedChange($id, $newStatus)
	{
		$this->houba->update($id,['arrived'=>$newStatus]);

		if ($this->isAjax()) {
			$this['datagridOverview']->redrawItem($id);
		}
	}

	public function handleDelete($id)
	{
		$this->houba->update($id,['invalidated'=>new Nette\Utils\DateTime()]);

		$this['datagridOverview']->reload();
	}

	public function handleSendPaymentMail($id)
	{
		if ($registration = $this->houba->findBy(['id'=>$id])->fetch()) {

			$mail = new Nette\Mail\Message();
			$mail->setFrom('david@ectabor.cz')
				->addTo($registration->email)
				->setSubject('Platba za Houbu 2019')
				->setHtmlBody('
					<p>Ahoj,<br><br>
					potvrzujeme, že nám přišla tvoje platba za Houbu. Děkujeme a těšíme se na tebe!<br>
					Pokud máš nějaký dotaz, stačí odpovědět na tento mail ;)
					</p>
					<p>S pozdravem za Táborskou mládež,<br>
					David Macek</p> 
				');

			try {
				$this->mailer->send($mail);
				$this->getPresenter()->redirect('resendMailSuccess');
			} catch (Nette\Mail\SmtpException $e) {
				\Tracy\Debugger::log($e);
				$this->getPresenter()->redirect('resendMailError');
			}

		} else {
			throw new Nette\InvalidArgumentException('Wrong registration ID was passed');
		}
	}

}
